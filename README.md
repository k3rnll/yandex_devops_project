Yandex project description

- чтобы запустить приложение и понять как оно работате использовал strace
- к счатью приложение на ключ ‘-h’ выдало справку как с ним работать, но не всю, падало при запуске с аргументами.
- нашел обращение открытия конфиг файла, создал конфиг.
- нашел обращение открытия лог файла, создал лог файл.
- в конфиге есть пункты для работы с базой данных, установил базу.
- приложение стало запускаться.
- аргументом ‘__’ приложение заполнило базу своими данными.
- обнаружил что приложение слушает порт 24596.
- после нескольких запросов к приложению, его лог файл стал неприлично большим, придумать как ратировать лог без возможной потери данных при перемещении файлов не получилось, поэтому временно вместо лог файла сделал софт линк на ‘/dev/null’.
- обнаружилось самопроизвольное падение приложения в интервале 20-30минут после старта, внешних причин не нашел, оно само так делает.
- стало понятно что нужен автоматический перезапуск, учитывая что нужна отказоусточивость и возможность дальнейшего масштабирования, решил использовать Docker Swarm за его простоту и легкость.
- собрал контейнер с приложением, запустил в одном стеке с базой данных, файлы базы расшарил в docker volume, чтобы безопасно перезапускать контейнер с базой.
- приложение работает в двух репликах, проверяется через healthcheck
- начал проверять эндпойнты руками, выяснилось, что приложение медленно стартует, через strace выяснил что оно отправляет запросы на адрес 8.8.8.8 и порт 80 и ждет ответа. С помощью iptables перенаправил трафик по этому адресу на адрес яндекса, приложение стало получать ответ и стартовать быстрее.
- начал разбираться с таймаутом по эндпойнту ‘/api/session’, включил логирование в настройках базы, нашел sql запрос, подключился через psql и проанализировал запрос через ‘explain (analyze)’, база на него отвечалаза две-три минуты, построчно сравнивая значения в разных таблицах, создал индексы по колонкам участвующим в запросе, запрос стал обрабатываться за 4-5 секунд.
- дальнейшее ручное тестирование выявило что приложение может работать, но на запросы отвечать не ожидаемым форматом сообщения, а писать ‘i feel bad’. тоже самое отвечало на эндпойнте ‘/ping’, сделал проверку healthchek по этому эндпойнту на ответ ‘pong’.
- начал прикручивать https с помощью nginx, добавил его в стек к приложению и базе, настроил как реверс прокси в оверлейной сети стека.
- познакомился с утилитой яндекс.танк, открылись глаза на поведение приложения в реальном времени.
- выяснил что nginx работает неккоректно перенаправляя запросы по оверлейной сети стека, до этого прямое обращение к ингрессу докер сварм, давало четкую балансировку round robin. чтобы не создавать отдельный хост только ради nginx, решил оставить его в стеке, но перенаправить его запросы на внешний адрес хоста. так пакет приходящий на порт 80 или 443 через докер ингресс попадал в контейнер nginx, из него выходил обратно в звено PREROUTING таблицы nat, и снова через ингресс и встроенный балансировщик сварма уже попадал в реплику приложения.
- эндпойнт /long_dummy, отвечал за две секунды, включил его кеширование.


* используются переменные гитлаба:
	PROD_HOST - ip целевого хоста
	ANSIBLE_SSH_KEY - (тип файл)
	ANSIBLE_USER 
* конфиг приложения:
	STUDENT_EMAIL
	DB_HOST
	DB_NAME
	DB_USER
	DB_PASSWORD
	DB_PORT
* сертификаты для nginx:
	NGINX_CERT - (тип файл)
	NGINX_KEY - (типа файл)


репозиторий настроен на работу с двумя доп ветками ‘dev’ и ‘infra’
- папка infrastructure, содержит ansible плейбуки для подготовки окружения и yaml конфиги для деплоя в docker swarm.
- папка app и balancer, докерфайл на сборку приложения и его конфиг, конфиг nginx.
- при создании ветки infra и создании изменений в папке infrastructure активируется пайплайн с ручным стартом, который по очереди:
    - сначала подготавливает хост установкой iptables, docker, дает необходимые права пользователю и рабочей папке проекта.
    - деплоит стек только с одной базой, чтобы она стартанула отдельно, без конфликтов
    - деплоит стек уже с одной репликой приложения, которая запускает процесс заполнения базы предварительными данными.
    - выполняется индексирование необходимых столбцов в таблицах базы.
- при создании ветки dev и изменении в папках app и balancer активируется автоматический пайплайн:
    - сборка из докерфайла приложения
    - выгрузка нового образа в реестр гитлаба
    - деплой стека с новой версией 

чтобы выполнить деплой на свеже созданный хост:
- обновить переменные если требуется
- в PROD_HOST указать новый адрес
- создать ветку infra и запушить
- создать ветку dev и запушить
